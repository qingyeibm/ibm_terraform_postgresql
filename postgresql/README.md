# POSTGRESQL

This repository performs the following tasks using Kubernetes and Helm:  
- Create a COS instance and a COS bucket  
- Create a Kubernetes PVC using the COS bucket as storage  
- Deploy PostgreSQL image to an existing VPC IKS cluster  
  
--- 

## Setup

You can add the original image below to your IBM registry as detailed in the Prerequisites section  
- https://docs.docker.com/engine/examples/postgresql_service/

---

### Prerequisites

- [Docker](https://www.docker.com/) running on your local machine

### Creating the PostgreSQL Image

A. Copy the Dockerfile from the link below to your local machine.
https://docs.docker.com/engine/examples/postgresql_service/
  
B. Build the docker image
$ docker build --network host --tag <region>.icr.io/<my_namespace>/<image_repo> .
Example:
```
$ docker build --network host --tag de.icr.io/my-test-ns/postgresql .
```
  
C. Push your image to the ICR Namespace. Make sure you're target the IBM Cloud region where you want to be able to access your new repository.
```
$ ibmcloud cr login
```
  
D. Push your image to the IBM Container Registry
```
$ docker push <region>.icr.io/<my_namespace>/<image_repo>:<tag>
```
  
E. Verify that your image was pushed successfully
```
ibmcloud cr image-list
```
  
---

## Resources

- S3FS
    - Create a Helm release for the IBM COS Plug-in 
    - Output the status of the helm release

- COS
    - Create a COS instances and a COS bucket
    - Create a COS service credential managed by IAM (Role: Manager)

- COS_PVC
    - Create a Persistent Volume Claim (PVC)
    - Create a Kubernetes secret for the service credentials

- DB
    - Create a Kubernetes secret for the PostgreSQL database
    - Deploy PostgreSQL image to an existing VPC IKS cluster
    - Create a temporary pod to grant write access to 'postgres' user

---

## Module Variables

Variable | Type | Description | Default
---------|------|-------------|--------
`ibmcloud_api_key` | String | The IBM Cloud platform API key required to deploy IAM enabled resources |
`generation` | String | Generation of VPC - 1 or 2 | `1`
`ibm_region` | String | IBM Cloud region where all resources will be deployed | `e.g. eu-de`
`resource_group` | String | Name for IBM Cloud Resource Group | `default`
`vpc_name` | String | Name of VPC where the cluster is running | ``
`cluster_name` | String | Name of the existing VPC cluster where image will be deployed | ``
`cos_name` | String | Cloud Object Storage instance name | ``
`cos_location` | String | Location name of the COS instance | `global`
`cos_plan` | String | Cloud Object Storage plan | `standard`
`HMAC` | String | HMAC parameter for COS instance | `true`
`cos_end_points` | String | The endpoints for the COS instance - public or private | `public`
`bucket_name` | String | COS bucket name | ``
`bucket_region` | String | IBM Cloud region where COS bucket will be deployed | `e.g. eu-de`
`bucket_storage_class` | String | Class for COS bucket storage | `standard`
`cos_access_key` | String | Name of COS access key | ``
`cos_service_role` | String | Service role of COS | `Manager`
`cos_secret` | String | Name of COS secret | ``
`pvc` | String | Name of the persistent volume claim | ``
`pvc_storage_class` | String | Storage class of the PVC to be created | ``
`pv_mount_path` | String | The mount path for the COS bucket | ``
`db_volume_name` | String | The volume name for the database secret | ``
`db_mount_path` | String | The mount path for the database secret | ``
`postgres_username` | String | postgres database user name | ``
`postgres_password` | String | Name of the persistent volume claim | ``
`app_name` | String | Name for app in Kubernetes | `postgresql`
`container_name` | String | Name for container in deployment | `postgresql`
`app_image` | String | Image to deploy - registry/registry-namespace/image | `de.icr.io/postgresql-test/postgresql:latest`
`db_secret` | String | Name of the database secret | ``
`namespace` | String | Kubernetes namespace to deploy application and create resources | `default`
