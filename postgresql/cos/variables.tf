##############################################################################
# Account variables
##############################################################################

variable resource_group_id {
    description = "Name for IBM Cloud Resource Group where resources will be deployed"
}

##############################################################################
# COS Variables
##############################################################################

variable cos_name {
   description = "Cloud Object Storage instance name"
}

variable cos_location {
   description = "Location name of the COS instance"
}

variable cos_plan {
   description = "Cloud Object Storage plan"
}

variable HMAC {
    description = "HMAC parameter for COS instance"
}

variable cos_end_points {
  description = "The endpoints for the COS instance - public or private"
}

variable bucket_name {
   description = "COS bucket name"
}

variable bucket_region {
  description = "IBM Cloud region where COS bucket will be deployed"
}

variable bucket_storage_class {
   description = "Class for COS bucket storage"
}

variable cos_access_key {
   description = "Name of COS access key"
}

variable cos_service_role {
   description = "Service role of COS"
}

##############################################################################
# Helm Variable
##############################################################################

variable helm_status {
    description = "Awaits the completion of the s3fs installation for the creation of COS resource key"
}

##############################################################################
