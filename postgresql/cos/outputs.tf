#########################################################################
#  COS bucket and Access key
#########################################################################

output cos_bucket_name {
   description = "COS bucket name"
   value       = ibm_cos_bucket.cos_bucket.bucket_name
}

output access_key {
   description = "Access Key"
   value       = element(split(":", ibm_resource_key.access.id),9)
}

#########################################################################