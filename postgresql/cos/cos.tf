##############################################################################
#  Create a COS instance 
##############################################################################

resource ibm_resource_instance cos_instance {

   name              = var.cos_name
   resource_group_id = var.resource_group_id
   service           = "cloud-object-storage"
   plan              = var.cos_plan
   location          = var.cos_location

   parameters = {
     "HMAC"            = var.HMAC
     service-endpoints = var.cos_end_points
   }

   //User can increase timeouts
   timeouts {
     create = "15m"
     update = "15m"
     delete = "15m"
  }

}

##############################################################################
#  Create a COS Access Key
##############################################################################

resource ibm_resource_key access {

   name                 = var.cos_access_key
   role                 = var.cos_service_role
   resource_instance_id = ibm_resource_instance.cos_instance.id
   parameters = {
    "HMAC"              = var.HMAC
  }

}

##############################################################################
#  Create a COS Bucket
##############################################################################

resource ibm_cos_bucket cos_bucket {

   bucket_name          = var.bucket_name
   resource_instance_id = ibm_resource_instance.cos_instance.id
   region_location      = var.bucket_region
   storage_class        = var.bucket_storage_class

}

#############################################################################

