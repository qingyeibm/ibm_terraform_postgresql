##############################################################################
#  Account variables
##############################################################################

variable ibmcloud_api_key {
   description = "The IBM Cloud platform API key required to deploy IAM enabled resources"
   type        = string
}

variable generation {
   description = "Generation of VPC - 1 or 2"
   type        = number
   default     = 1
}

variable ibm_region {
   description = "IBM Cloud region where all resources will be deployed"
   type        = string
}

variable resource_group {
   description = "Name for IBM Cloud Resource Group where resources will be deployed"
   type        = string
}


##############################################################################
#  VPC Variables
##############################################################################

variable vpc_name {
   description = "Name of VPC where the cluster is running"
   type        = string
}

#############################################################################
#  Cluster Variables
##############################################################################

variable cluster_name {
   description = "Name of the existing VPC cluster where image will be deployed"
   type        = string
}

##############################################################################
# COS Variables
##############################################################################
variable cos_name {
   description = "Cloud Object Storage instance name"
   type        = string
}

variable cos_location {
   description = "Location name of the COS instance"
   default     = "global"
}

variable cos_plan {
   description = "Cloud Object Storage plan"
   default     = "standard"
}

variable HMAC {
    description = "HMAC parameter for COS instance"
    type        = string
    default     = "true"
}

variable cos_end_points {
   description = "The endpoints for the COS instance - public or private"
   type        = string
   default     = "public"
}

variable bucket_name {
   description = "COS bucket name"
   type        = string
}

variable bucket_region {
  description = "IBM Cloud region where COS bucket will be deployed"
   type       = string
}

variable bucket_storage_class {
   description = "Class for COS bucket storage"
   type        = string
   default     = "standard"
}

variable cos_access_key {
   description = "Name of COS access key"
   type        = string
}

variable cos_secret {
   description = "Name of COS secret"
   type        = string
}

variable cos_service_role {
   description = "Service role of COS"
   type        = string
   default     = "Manager"
}

##################################################################################
#  PVC Variables
##################################################################################

variable pvc {
   description = "Name of the persistent volume claim"
   type        = string
}

variable pvc_storage_class {
   description = "Storage class of the PVC to be created"
   default     = "ibmc-s3fs-standard-perf-regional"
}

variable pv_mount_path {
   description = "The mount path for the COS bucket"
   type        = string
}

variable db_volume_name {
   description = "The volume name for the database secret"
   type        = string
}

variable db_mount_path {
   description = "The mount path for the database secret"
   type        = string
}

##################################################################################
#  Database Variables
##################################################################################

variable postgres_username {
   description = "postgres database user name"
   type        = string
}

variable postgres_password {
   description = "postgres database password"
   type        = string
}

variable app_name {
   description = "Name for app in Kubernetes"
   type        = string
   default     = "postgresql"
}

variable container_name {
   description = "Name for container in deployment"
   type        = string
   default     = "postgresql"
}

variable app_image {
   description = "Image to deploy - registry/registry-namespace/image"
   type        = string
   default     = "de.icr.io/postgresql-test/postgresql:latest"
}

variable db_secret {
   description = "Name of the database secret"
   type        = string
}

variable namespace {
   description = "Kubernetes namespace to deploy application and create resources"
   type        = string
   default     = "default"
}

##################################################################################