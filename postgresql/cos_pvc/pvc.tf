##############################################################################
#  Fetch Secret With External Resource
##############################################################################

data ibm_iam_auth_token tokendata {}

data external cos_hmac_keys {

   program = [
      "bash",
      "${path.module}/config/fetch_hmac_key.sh",
      data.ibm_iam_auth_token.tokendata.iam_access_token,
      var.access_key
   ]

}


##############################################################################
#  Kubernetes PVC
##############################################################################

resource kubernetes_persistent_volume_claim cos_pvc {

   metadata {
      name      = var.pvc
      namespace = var.namespace
      annotations = {
         "ibm.io/auto-create-bucket" = "false"
         "ibm.io/auto-delete-bucket" = "false"
         "ibm.io/bucket"             = var.bucket_name
         "ibm.io/endpoint"           = "https://s3.direct.${var.ibm_region}.cloud-object-storage.appdomain.cloud"
         "ibm.io/secret-name"        = kubernetes_secret.cos_write_access.metadata.0.name
      }
   }
   spec {
      access_modes = ["ReadWriteOnce"]
      resources {
        requests = {
          storage = "8Gi"
        }
      }
      storage_class_name = var.pvc_storage_class
   }

}

data external persistent_volume {

   program = [
      "bash",
      "${path.module}/config/get_pv_name.sh",
      var.pvc
   ]
   depends_on = [ kubernetes_persistent_volume_claim.cos_pvc ]

}

##############################################################################
#  Create IKS Secret for COS Creds
##############################################################################

resource kubernetes_secret cos_write_access {

   metadata {
     name = var.access_key
     namespace = var.namespace 
   }

   data = {
      "access-key" = data.external.cos_hmac_keys.result["access_key_id"]
      "secret-key" = data.external.cos_hmac_keys.result["secret_access_key"]
   }

   type = "ibm/ibmc-s3fs"

}

##############################################################################
