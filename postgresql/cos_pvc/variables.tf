##############################################################################
# Account Variables
##############################################################################

variable ibm_region {
    description = "Region where COS buckets will be provisioned"
}

variable resource_group_id {
    description = "ID of resource group where COS will be provisioned"
}


##############################################################################
# COS Variables
##############################################################################

variable bucket_name {
    description = "COS bucket name"
}

variable access_key {
   description = "Name of COS access key"
}

variable cos_secret {
   description = "Name of COS secret"
}

##############################################################################
# PVC Variables
##############################################################################

variable pvc {
   description = "Name of the persistent volume claim"
}

variable namespace {
   description = "Kubernetes namespace where resources to be created"
}

variable pvc_storage_class {
    description = "Storage class of the PVC to be created"
}

##############################################################################
