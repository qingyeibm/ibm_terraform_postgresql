PVC=$1
PV=$(kubectl get pvc $PVC -o "jsonpath"='{.spec.volumeName}')

jq -n '{
    "mount_volume_name" : "'$PV'"
}'

