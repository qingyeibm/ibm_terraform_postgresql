#######################################################################
#  PV and PVC outputs
#######################################################################

output volume_name {
  value = data.external.persistent_volume.result["mount_volume_name"]
}

output pvc_id {
  value = kubernetes_persistent_volume_claim.cos_pvc.id
}

#######################################################################