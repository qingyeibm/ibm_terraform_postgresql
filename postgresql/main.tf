##########################################################################################
#  IBM Provider
##########################################################################################

provider ibm {
   ibmcloud_api_key = var.ibmcloud_api_key
   region           = var.ibm_region
   generation       = var.generation
   ibmcloud_timeout = 60
}

##########################################################################################
#  Resource Group
##########################################################################################

data ibm_resource_group group {
   name = var.resource_group
}

##########################################################################################
#  Kubernetes Provider
##########################################################################################

provider kubernetes {
   load_config_file       = false
   host                   = data.ibm_container_cluster_config.cluster.host
   client_certificate     = data.ibm_container_cluster_config.cluster.admin_certificate
   client_key             = data.ibm_container_cluster_config.cluster.admin_key
   cluster_ca_certificate = data.ibm_container_cluster_config.cluster.ca_certificate
   config_context_cluster = var.cluster_name
}

##########################################################################################
#  Cluster Data
##########################################################################################

data ibm_container_vpc_cluster cluster {
   cluster_name_id   = var.cluster_name
   resource_group_id = data.ibm_resource_group.group.id
}

data ibm_container_cluster_config cluster {
   cluster_name_id   = var.cluster_name
   resource_group_id = data.ibm_resource_group.group.id
   admin             = true
}

##########################################################################################
#  S3FS Plug-in
##########################################################################################

module s3fs {
   source      = "./s3fs_install"

   namespace   = var.namespace
}

##########################################################################################
#  COS
##########################################################################################

module cos {
   source               = "./cos"

   cos_name             = var.cos_name
   helm_status          = module.s3fs.status
   resource_group_id    = data.ibm_resource_group.group.id
   cos_plan             = var.cos_plan  #"standard"
   cos_location         = var.cos_location #"global"
   HMAC                 = var.HMAC
   cos_end_points       = var.cos_end_points
   bucket_name          = var.bucket_name
   bucket_region        = var.bucket_region
   bucket_storage_class = var.bucket_storage_class
   cos_access_key       = var.cos_access_key
   cos_service_role     = var.cos_service_role
}

##########################################################################################
#  PVC
##########################################################################################

module cos_pvc {
   source            = "./cos_pvc"

   ibm_region        = var.ibm_region
   namespace         = var.namespace
   bucket_name       = var.bucket_name
   resource_group_id = data.ibm_resource_group.group.id
   access_key        = module.cos.access_key
   cos_secret        = var.cos_secret
   pvc               = var.pvc
   pvc_storage_class = var.pvc_storage_class

}

##########################################################################################
#  Database
##########################################################################################

module db {
   source            = "./db"

   pvc               = var.pvc
   namespace         = var.namespace
   volume_name       = module.cos_pvc.volume_name
   pv_mount_path     = var.pv_mount_path
   db_username       = var.postgres_username
   db_password       = var.postgres_password
   db_mount_path     = var.db_mount_path
   db_volume_name    = var.db_volume_name
   app_name          = var.app_name
   container_name    = var.container_name
   app_image         = var.app_image
   db_secret         = var.db_secret

}

##########################################################################################
