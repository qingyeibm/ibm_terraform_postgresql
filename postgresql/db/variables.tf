#####################################################################################
# PVC Variables
#####################################################################################

variable pvc {
   description = "Persistent Volume Claim name"
}

variable volume_name {
   description = "Persistent Volume name"
}

#####################################################################################
# Database Variables
#####################################################################################

variable app_name {
   description = "Name for app in Kubernetes"
}

variable namespace {
   description = "Kubernetes namespace to deploy application"
}

variable container_name {
   description = "Name for container in deployment"
}

variable app_image {
   description = "Image to deploy - registry/registry-namespace/image"
}

variable db_username {
   description = "Database user name"
}

variable db_password {
   description = "Database password"
}

variable db_secret {
   description = "Name of the database secret"
}

variable pv_mount_path {
   description = "The mount path for the COS bucket"
}

variable db_volume_name {
   description = "The volume name for the database secret"
}

variable db_mount_path {
   description = "The mount path for the database secret"
}

#####################################################################################