############################################################################################
#  Create Secret for PostgreSQL Database
############################################################################################

resource kubernetes_secret db_secret {
   metadata {
      name      = var.db_secret
      namespace = var.namespace
   }

   data = {
      username = var.db_username
      password = var.db_password
   }

   type = "Opaque"
}

############################################################################################
#  PostgreSQL Deployment
############################################################################################

resource kubernetes_deployment postgresql {
   metadata {
      name      = var.app_name
      namespace = var.namespace

      labels = {
         run = var.app_name
      }
   }

   spec {
      replicas = "1"
      selector {
         match_labels = {
            run = var.app_name
         }
      }

      template {
         metadata {
            labels = {
               run = var.app_name
            }
         }

         spec {
           container {
              name  = var.container_name
              image = var.app_image
              volume_mount {
                 mount_path = var.db_mount_path
                 name = var.db_volume_name
              }
              volume_mount {
                 mount_path = var.pv_mount_path
                 name = var.volume_name
              }
           }
           volume {
              name = var.db_volume_name
              secret {
                 secret_name = var.db_secret
              }
           }
           volume {
              name = var.volume_name
              persistent_volume_claim {
                 claim_name = var.pvc
              }
           }
         }
      }
   }
}

resource null_resource permission_pod {
   provisioner local-exec {
      command = <<BASH
kubectl apply -f - <<EOF
apiVersion: v1
kind: Pod
metadata:
   name: prepare-permission
spec:
   containers:
   - name: prepare-permission
     image: busybox
     command: ['/bin/sh', '-c']
     args: ['if [ ! -d ${var.pv_mount_path}/postgres ]; then mkdir ${var.pv_mount_path}/postgres; fi;chown -R 106 ${var.pv_mount_path}/postgres']
     volumeMounts:
     - name: "${var.volume_name}"
       mountPath: "${var.pv_mount_path}"
   volumes:
   - name: "${var.volume_name}"
     persistentVolumeClaim:
        claimName: "${var.pvc}"
EOF
sleep 8
kubectl delete pod prepare-permission
      BASH
   }
   depends_on = [kubernetes_deployment.postgresql]
}

############################################################################################
