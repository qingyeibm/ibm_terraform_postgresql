##############################################################################
# Install IBM COS Plug-in Charts
##############################################################################

resource helm_release s3fs {
  name       = "ibm-cos-plugin"
  chart      = "${path.module}/charts/ibm-object-storage-plugin"
  version    = "1.1.4"
  namespace  = var.namespace
}

output status {
  value = helm_release.s3fs.status
}

variable namespace {
  description = "Kubernetes namespace to create Helm release"
}

##############################################################################

